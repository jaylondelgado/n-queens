from pprint import pprint

# def is_in_danger(board, row_index=2, column_index=2):
def is_in_danger(board, row_index, column_index):
    # Check the row for a rook
    for index, value in enumerate(board[row_index]):
        # print('index', index)
        # print('col', value)
        # print('-------------')
        if value == 0 or index == column_index:
            pass
        else:
            return True
    


def place_rook_in_column(board, column_number, n):
    if column_number >= n: # Base case (ran out of board)
        return
    for row_number in range(n): # Keep trying rows until we find one
        print('row_number',row_number)
        board[row_number][column_number] = 5

        # if is_in_danger(board, row_number = 2, column_number = 2:
        if is_in_danger(board, row_number, column_number):
            board[row_number][column_number] = 0 # Removing it from the board
        else:
            place_rook_in_column(board, column_number + 1, n)

def four_rooks(n):
    board = [[0] * n for i in range(n)]
    # board = [
    #     [0, 0, 0, 0, 0],
    #     [0, 0, 5, 0, 0],
    #     [0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0],
    # ]

    place_rook_in_column(board, 0, n)
    return board
    
if __name__ == "__main__":
    board = four_rooks(10)

pprint(four_rooks(10))